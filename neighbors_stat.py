


import time

MAX_LOAD = 1000*1000## big

class Neighbor(object):
    def __init__(self):
        self.load = MAX_LOAD
        self.last_update = 0

    def __str__(self):
        return str(self.load)+" update:"+str(self.last_update)

class NeighborStat(object):
    def __init__(self):
        self.neighbers = {}
    
    def add(self, peer, load):
        if peer not in self.neighbers:
            self.neighbers[peer] = Neighbor()
        n = self.neighbers[peer]
        n.load = load
        n.last_update = time.time()

    def delete(self, peer):
        self.neighbers.pop(peer)
    
    def choose_one(self, my_load):
        if my_load <= 1:
            return None
        min_load_key = None
        for peer, neighber in self.neighbers.iteritems():
            if min_load_key == None or neighber.load < self.neighbers[min_load_key].load:
                min_load_key = peer
        #if self.neighbers[min_load_key].load*2 < my_load and my_load - self.neighbers[min_load_key].load > 1:
        if my_load - self.neighbers[min_load_key].load > 1:
            return min_load_key
        else:
            return None

    def __str__(self):
        l = ["\n"]
        for peer, neighber in self.neighbers.iteritems():
            l.append( str(peer) + "->" + str(neighber))
        l.append("\n")
        return '\n'.join(l)
