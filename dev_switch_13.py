
import os
import json
import time
import random
from oslo.config import cfg

from ryu.base import app_manager
from ryu.controller import ofp_event
#from ryu.controller import  controller
from ryu.controller.handler import CONFIG_DISPATCHER, MAIN_DISPATCHER, HANDSHAKE_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.ofproto import ofproto_v1_3
from ryu.lib.packet import packet
from ryu.lib.packet import ethernet
from ryu import utils
from ryu.lib import hub

from controller_peers import ControllerPeers,ControllerPeerClient
import peer_event
import role_changing_status as rcs
from neighbors_stat import NeighborStat

_opts = [  
    cfg.StrOpt('peer_bind_host', default='0.0.0.0'),  
    cfg.IntOpt('peer_bind_port', default=9292),  
    cfg.IntOpt('to_be_master', default=''),  
]  
  
CONF = cfg.CONF  
CONF.register_opts(_opts)  
CONF(default_config_files=['controller.conf'])

def info_datapath(datap):
    #return ' '.join(['addr', str(datap.address), 'port', str(datap.ports), 'id:', str(datap.id) , 'xid', str(datap.xid)])
    return ' '.join(['addr', str(datap.address), 'id:', str(datap.id) , 'xid', str(datap.xid)])



class DistributedSimpleSwitch(app_manager.RyuApp):
    OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]

    def __init__(self, *args, **kwargs):
        super(DistributedSimpleSwitch, self).__init__(*args, **kwargs)
        self.mac_to_port = {}
        '''status
        '''
        self.mst_dps = {}
        self.eq_dps = {}
        self.generation_id = 0
        self.neighbors = NeighborStat()
        self.role_chaning = None
        self.call_in_count = 0
        self.dumy_priority =123
        self.equal_cache_q = None #hub.Queue(16)
            
    def get_load(self):
        return len(self.mst_dps)

    def start(self):
        super(DistributedSimpleSwitch, self).start()
        ## read init conf file
        self.my_name = os.getenv('cid', '100')
        if self.my_name == '1':
            self.generation_id = 1000
            self.logger.info('To be a init Master')
            return hub.spawn(ControllerPeers())
        else:
            self.logger.info('NOT to be a partner')
            return hub.spawn(ControllerPeerClient("127.0.0.1",6737))

    def setup_by_conf(self):
        js = json.load(open('init.json'))
        roles = js[0]
        peer_ports = js[1]

    def isMaster(self, dp):
        return dp.id in self.mst_dps

    def isEqual(self, dp):
        return dp.id not in self.mst_dps and self.role_chaning != None

    def getOneDp(self):
        _dp = None
        for dp_id, dp in self.mst_dps.iteritems():
            _dp = dp
        return _dp

    @set_ev_cls(ofp_event.EventOFPEchoRequest,[HANDSHAKE_DISPATCHER, CONFIG_DISPATCHER, MAIN_DISPATCHER])
    def echo_request_handler(self, ev):
        self.logger.debug('OFPEchoRequest received, set self.role_chaning.dp')

    @set_ev_cls(ofp_event.EventOFPSwitchFeatures, CONFIG_DISPATCHER)
    def switch_features_handler(self, ev):
        datapath = ev.msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        print "switch_features_handler get datapath:",info_datapath(datapath)
        if self.my_name != '1':
            self.send_role_request(datapath)
            self.eq_dps[datapath.id] = datapath
            return
        ## init master shoud do
        self.mst_dps[datapath.id] = datapath
        self.send_role_request(datapath, ofproto.OFPCR_ROLE_MASTER)
        # install table-miss flow entry
        #
        # We specify NO BUFFER to max_len of the output action due to
        # OVS bug. At this moment, if we specify a lesser number, e.g.,
        # 128, OVS will send Packet-In with invalid buffer_id and
        # truncated packet data. In that case, we cannot output packets
        # correctly.
        match = parser.OFPMatch()
        actions = [parser.OFPActionOutput(ofproto.OFPP_CONTROLLER,
                                          ofproto.OFPCML_NO_BUFFER)]
        self.add_flow(datapath, 0, match, actions)

    def add_flow(self, datapath, priority, match, actions, flags=0, hard_timeout=0, commd=0):
        #if self.role != 'master':
        if not self.isMaster(datapath):
            return
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        inst = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS,
                                             actions)]
        mod = parser.OFPFlowMod(datapath=datapath, priority=priority,
                                match=match, instructions=inst, flags=flags, hard_timeout=hard_timeout)
        if commd != 0:
            mod = parser.OFPFlowMod(datapath=datapath, priority=priority,
                                match=match, instructions=inst, flags=flags, hard_timeout=hard_timeout, command=commd) 
        datapath.send_msg(mod)
        self.logger.debug("add_flow:"+str(mod))

    @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    def _packet_in_handler(self, ev):
        msg = ev.msg
        datapath = msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        in_port = msg.match['in_port']

        pkt = packet.Packet(msg.data)
        eth = pkt.get_protocols(ethernet.ethernet)[0]

        dst = eth.dst
        src = eth.src

        dpid = datapath.id
        self.mac_to_port.setdefault(dpid, {})

        #print "_packet_in_handler get datapath:",info_datapath(datapath)
        #self.logger.info("packet in %s %s %s %s", dpid, src, dst, in_port)

        # learn a mac address to avoid FLOOD next time.
        self.mac_to_port[dpid][src] = in_port

        if dst in self.mac_to_port[dpid]:
            out_port = self.mac_to_port[dpid][dst]
        else:
            out_port = ofproto.OFPP_FLOOD

        actions = [parser.OFPActionOutput(out_port)]

        # install a flow to avoid packet_in next time
        if out_port != ofproto.OFPP_FLOOD:
            match = parser.OFPMatch(in_port=in_port, eth_dst=dst)
            #self.add_flow(datapath, 1, match, actions)

        data = None
        if msg.buffer_id == ofproto.OFP_NO_BUFFER:
            data = msg.data

        out = parser.OFPPacketOut(datapath=datapath, buffer_id=msg.buffer_id,
                                  in_port=in_port, actions=actions, data=data)
        if not self.isMaster(datapath):
            if self.role_chaning and self.role_chaning.stat() == rcs.STAT_RECV_FLOE_DEL_MSG:
                #cache
                self.equal_cache_q.put(out)
                self.logger.debug("equal begin to cache packin:%d" % self.equal_cache_q.qsize())
            #else:
            #    self.logger.debug("packet in, %s role:%s, then ignoring", datapath.id, "some role")
            return
        #it is master now 
        if self.role_chaning and self.role_chaning.stat() == rcs.STAT_RECV_FLOE_DEL_MSG:
            self.logger.debug("after recv dumy flow del msg, old master stop processing packet in msg")
            return
        while self.equal_cache_q and not self.equal_cache_q.empty():
            datapath.send_msg(self.equal_cache_q.get())
        self.logger.info("process packet in %s %s %s %s", dpid, src, dst, in_port)
        datapath.send_msg(out)

    @set_ev_cls(ofp_event.EventOFPErrorMsg, [HANDSHAKE_DISPATCHER, CONFIG_DISPATCHER, MAIN_DISPATCHER])
    def error_msg_handler(self, ev):
        msg = ev.msg
        self.logger.debug('OFPErrorMsg received: type=0x%02x code=0x%02x '
                  'message=%s',
                  msg.type, msg.code, utils.hex_array(msg.data))

    def send_role_request(self, datapath, _role=None):
        ofp = datapath.ofproto
        if _role is None:
            _role = ofp.OFPCR_ROLE_NOCHANGE
        ofp_parser = datapath.ofproto_parser
        req = ofp_parser.OFPRoleRequest(datapath, _role, self.generation_id+1)
        datapath.send_msg(req)

    @set_ev_cls(ofp_event.EventOFPRoleReply, MAIN_DISPATCHER)
    def role_reply_handler(self, ev):
        msg = ev.msg
        datapath = msg.datapath
        ofp = datapath.ofproto
        _role = ''
        if msg.role == ofp.OFPCR_ROLE_NOCHANGE:
            pass
        elif msg.role == ofp.OFPCR_ROLE_EQUAL:
            _role = 'equal'
            if self.role_chaning and self.role_chaning.stat() == rcs.STAT_NORMAL:
                self.role_chaning.next() # to STAT_CHANGING
        elif msg.role == ofp.OFPCR_ROLE_MASTER:
            _role = 'master'
        elif msg.role == ofp.OFPCR_ROLE_SLAVE:
            _role = 'slave'
        else:
            _role = 'unknown'
        if self.isMaster(datapath) and _role != '' and _role != 'master':
            self.eq_dps[datapath.id] = self.mst_dps.pop(datapath.id)
            self.role_chaning = None
        if not self.isMaster(datapath) and _role == 'master':
            self.mst_dps[datapath.id] = self.eq_dps.pop(datapath.id)
            self.role_chaning = None
            self.logger.debug("TIME:" + str(time.time()) + ", grant master for swid:"+str(datapath.id))
        self.generation_id = msg.generation_id
        self.logger.debug('OFPRoleReply received: '
                  'role=%s generation_id=%d',
                  _role, msg.generation_id)

    @set_ev_cls(ofp_event.EventOFPErrorMsg, [HANDSHAKE_DISPATCHER, CONFIG_DISPATCHER, MAIN_DISPATCHER])
    def echo_request_handler(self, ev):
        msg = ev.msg
        datapath = msg.datapath
        ofp = datapath.ofproto
        self.logger.debug("rec echo request in 13")

    def send_table_stats_request(self, datapath):
        ofp = datapath.ofproto
        ofp_parser = datapath.ofproto_parser
        req = ofp_parser.OFPTableStatsRequest(datapath, 0)
        datapath.send_msg(req)

    @set_ev_cls(ofp_event.EventOFPTableStatsReply, MAIN_DISPATCHER)
    def table_stats_reply_handler(self, ev):
        tables = []
        for stat in ev.msg.body:
            tables.append('table_id=%d active_count=%d lookup_count=%d matched_count=%d' % (stat.table_id,
                                stat.active_count,
                                stat.lookup_count,
                                stat.matched_count))
        self.logger.debug('TableStats:%s',tables)

    def send_flow_stats_request(self, datapath):
        ofp = datapath.ofproto
        ofp_parser = datapath.ofproto_parser
        cookie = cookie_mask = 0
        req = ofp_parser.OFPFlowStatsRequest(datapath, 0,
            ofp.OFPTT_ALL,
            ofp.OFPP_ANY,
            ofp.OFPG_ANY,
            cookie,
            cookie_mask,
            self.dumy_match)
        datapath.send_msg(req)

    @set_ev_cls(ofp_event.EventOFPFlowStatsReply, MAIN_DISPATCHER)
    def flow_stats_reply_handler(self, ev):
        flows = []
        for stat in ev.msg.body:
            flows.append('table_id=%s '
                         'duration_sec=%d duration_nsec=%d '
                         'priority=%d '
                         'idle_timeout=%d hard_timeout=%d flags=0x%04x '
                         'cookie=%d packet_count=%d byte_count=%d '
                         'match=%s instructions=%s' %
                         (stat.table_id,
                          stat.duration_sec, stat.duration_nsec,
                          stat.priority,
                          stat.idle_timeout, stat.hard_timeout, stat.flags,
                          stat.cookie, stat.packet_count, stat.byte_count,
                          stat.match, stat.instructions))
        self.logger.debug('FlowStats: %s', flows)

    @set_ev_cls(ofp_event.EventOFPBarrierReply, MAIN_DISPATCHER)
    def barrier_reply_handler(self, ev):
        self.logger.debug("||||||||||||rec barrier_reply")
        msg = ev.msg
        datapath = msg.datapath
        if self.isMaster(datapath):
            if self.role_chaning.stat() == rcs.STAT_ADD_FLOW_DONE:
                self.role_chaning.next() # to STAT_BARRIER_AFTER_ADD_FLOW
                self.logger.debug("|||>>>master role_chaning: STAT_BARRIER_AFTER_ADD_FLOW")
                #delete dumy flow
                ofp = self.role_chaning.dp.ofproto
                self.add_flow(self.role_chaning.dp, self.dumy_priority, self.dumy_match, self.dumy_actions, commd=ofp.OFPFC_DELETE)
            if self.role_chaning.stat() == rcs.STAT_RECV_FLOE_DEL_MSG:
                #notify peer to change to new master
                ''' do it in call in
                _msg = peer_event.PeerMsg(self.my_name, _reply_type, {'role':self.role,})
                peer_obj.send_msg(_msg)'''
                self.role_chaning.next() # to STAT_NOTIFY_PEER,
        
    def _add_dumy_flow(self):
        parser = self.role_chaning.dp.ofproto_parser
        ofp = self.role_chaning.dp.ofproto
        self.dumy_actions = [parser.OFPActionOutput(ofp.OFPP_FLOOD)]
        # install a flow
        self.dumy_match = parser.OFPMatch(in_port=1,
                eth_src='00:ff:ff:00:00:00', eth_dst='00:ff:ff:ff:00:00')
        self.add_flow(self.role_chaning.dp, self.dumy_priority, self.dumy_match, self.dumy_actions, flags=ofp.OFPFF_SEND_FLOW_REM, hard_timeout=10)
        #then send Barrier Request  to seperate add flow
        req = parser.OFPBarrierRequest(self.role_chaning.dp)
        self.role_chaning.dp.send_msg(req)
        #query flow table
        #self.send_flow_stats_request(self.role_chaning.dp)

    @set_ev_cls(ofp_event.EventOFPFlowRemoved, MAIN_DISPATCHER)
    def flow_removed_handler(self, ev):
        msg = ev.msg
        dp = msg.datapath
        ofp = dp.ofproto
        if msg.reason == ofp.OFPRR_IDLE_TIMEOUT:
            reason = 'IDLE TIMEOUT'
        elif msg.reason == ofp.OFPRR_HARD_TIMEOUT:
            reason = 'HARD TIMEOUT'
        elif msg.reason == ofp.OFPRR_DELETE:
            reason = 'DELETE'
        elif msg.reason == ofp.OFPRR_GROUP_DELETE:
            reason = 'GROUP DELETE'
        else:
            reason = 'unknown'
        self.logger.debug('>>>>>>>>>OFPFlowRemoved received: '
                          'cookie=%d priority=%d reason=%s table_id=%d '
                          'duration_sec=%d duration_nsec=%d '
                          'idle_timeout=%d hard_timeout=%d '
                          'packet_count=%d byte_count=%d match.fields=%s',
                          msg.cookie, msg.priority, reason, msg.table_id,
                          msg.duration_sec, msg.duration_nsec,
                          msg.idle_timeout, msg.hard_timeout,
                          msg.packet_count, msg.byte_count, msg.match)
        if self.isMaster(dp) and self.role_chaning.stat() == rcs.STAT_BARRIER_AFTER_ADD_FLOW:
            self.logger.debug('master recv flow delete msg')
            #then send Barrier Request, make sure the packets before DEL_MSG have been processed
            parser = self.role_chaning.dp.ofproto_parser
            req = parser.OFPBarrierRequest(self.role_chaning.dp)
            self.role_chaning.dp.send_msg(req)
            self.role_chaning.next() # to STAT_RECV_FLOE_DEL_MSG
        if self.isEqual(dp) and self.role_chaning.stat() == rcs.STAT_CHANGING:
            self.logger.debug('equal recv flow delete msg')
            #set up cache queue
            self.equal_cache_q = hub.Queue(26)
            self.role_chaning.next() # to STAT_RECV_FLOE_DEL_MSG

    def react_to_peer(self, peer_obj, msg_obj):
        getattr(self,'_do_msg'+str(msg_obj.msg_type))(peer_obj, msg_obj)

    def broadcast(self, peer):
        broadcast_msg = peer_event.PeerMsg(self.my_name, peer_event.PEER_MSG_CALL_IN , {'load': self.get_load(),})
        peer.send_msg(broadcast_msg)

    def _do_msg90(self, peer_obj, msg_obj):
        self.call_in_count += 1
        self.neighbors.add(peer_obj, int(msg_obj.info['load']))
        #self.logger.debug('call in from:' + str(peer_obj) + "self.role_chaning:"+str(self.role_chaning))
        if self.call_in_count % random.randint(2,4) == 0 and self.role_chaning is None:
            '''master ask a peer to begin '''
            choosen = self.neighbors.choose_one(self.get_load())
            #self.logger.debug('choosen:'+ str(choosen))
            #self.logger.debug('choosen eq peer_obj:'+ str(str(choosen) == str(peer_obj)) )
            if str(choosen) == str(peer_obj):
                self.role_chaning = rcs.StateMachine('master')
                self.role_chaning.peer = peer_obj
                ##init self.role_chaning.dp
                self.role_chaning.dp = self.getOneDp()
                _msg = peer_event.PeerMsg(self.my_name, peer_event.PEER_MSG_INIT_Q
                        , {'role': 'master', 'swid': self.role_chaning.dp.id} )
                peer_obj.send_msg(_msg) 
                self.logger.debug("TIME:" + str(time.time()) +',master choose swid:' + str(self.role_chaning.dp.id)) 
        if self.role_chaning and str(self.role_chaning.peer) == str(peer_obj) and self.isMaster(self.role_chaning.dp):
            if self.role_chaning.stat() == rcs.STAT_CHANGING:
                #add dumy flow
                self._add_dumy_flow() 
                self.role_chaning.next() #to STAT_ADD_FLOW_DONE
            if self.role_chaning.stat() == rcs.STAT_NOTIFY_PEER:
                _msg = peer_event.PeerMsg(self.my_name, peer_event.PEER_MSG_ASK_CHANGE_TO_MASTER, {'role': 'master',})
                peer_obj.send_msg(_msg)
                self.role_chaning.next() # back to normal
                ##clear migrated dp
                self.eq_dps[self.role_chaning.dp.id] = self.mst_dps.pop(self.role_chaning.dp.id)
                self.role_chaning = None

    def _do_msg100(self, peer_obj, msg_obj):
            #PEER_MSG_INIT_Q = 100 
            #only partner recv this msg
            self.logger.debug('recv from peer:'+ str(msg_obj))
            _reply_type = peer_event.PEER_MSG_INIT_Reject
            _peer_role = msg_obj.info['role']
            _dp_id = int(msg_obj.info['swid'])
            self.logger.debug('partner recv migrating swid:' + str(_dp_id)) 
            self.logger.debug(self.eq_dps) 
            if _dp_id in self.eq_dps:
                _reply_type = peer_event.PEER_MSG_INIT_Agree 
                self.role_chaning = rcs.StateMachine('equal') 
                self.role_chaning.dp = self.eq_dps[_dp_id]
                self.role_chaning.peer = peer_obj
            _msg = peer_event.PeerMsg(self.my_name, _reply_type, {'role': 'equal',})
            peer_obj.send_msg(_msg)
            if self.role_chaning: #begin to change role
                #change to equal
                ofp = self.role_chaning.dp.ofproto
                self.send_role_request(self.role_chaning.dp, ofp.OFPCR_ROLE_EQUAL)

    def _do_msg101(self, peer_obj, msg_obj):
        #PEER_MSG_INIT_Reject
        self.role_chaning = None
        self.logger.debug('recv init reject msg, reset all')


    def _do_msg102(self, peer_obj, msg_obj):
        #PEER_MSG_INIT_Agree
        # master recv this msg
        if self.role_chaning and self.role_chaning.stat() == rcs.STAT_NORMAL:
            self.role_chaning.next()
        else:
            self.logger.debug('iilegal stat for PEER_MSG_INIT_Agree')

    def _do_msg200(self, peer_obj, msg_obj):
        #PEER_MSG_ASK_CHANGE_TO_MASTER
        #only partner recv this
        if self.role_chaning and self.role_chaning.stat() == rcs.STAT_RECV_FLOE_DEL_MSG:
            self.logger.debug('recv PEER_MSG_ASK_CHANGE_TO_MASTER in the equal')
            ofp = self.role_chaning.dp.ofproto
            self.send_role_request(self.role_chaning.dp, ofp.OFPCR_ROLE_MASTER)
            self.role_chaning.next()#STAT_SEND_MASTER_ROLE_R,

