




STAT_NORMAL = 99

STAT_MASTER = 1
STAT_NO_MASTER = 2
STAT_EQUAL = 3


STAT_CHANGING =  10
STAT_ADD_FLOW_DONE = 12
STAT_BARRIER_AFTER_ADD_FLOW = 13
STAT_RECV_FLOE_DEL_MSG = 14
STAT_BARRIER_AFTER_PROCESS = 15
STAT_NOTIFY_PEER  = 16


STAT_SEND_MASTER_ROLE_R =20
STAT_REC_MASTER_ROLE_C= 21

STAT_ERR = 100



'''
Master --> Slave
'''
Stats_Changing_2Slave = (
    STAT_NORMAL,
    STAT_CHANGING, #after send init msg
    STAT_ADD_FLOW_DONE,
    STAT_BARRIER_AFTER_ADD_FLOW,
    STAT_RECV_FLOE_DEL_MSG,
    #STAT_BARRIER_AFTER_PROCESS,
    STAT_NOTIFY_PEER,
    #back to STAT_NORMAL
)

'''
Slave to Master
'''
Stats_Changing_2Master = (
    STAT_NORMAL,
    STAT_CHANGING, #after rec init msg
    STAT_RECV_FLOE_DEL_MSG,
    STAT_SEND_MASTER_ROLE_R,
    STAT_REC_MASTER_ROLE_C,
    # back to STAT_NORMAL
)




class StateMachine(object):
    def __init__(self, curr_role):
        self.fsm = Stats_Changing_2Master
        self.curr_role = curr_role
        if self.curr_role == 'master':
            self.fsm = Stats_Changing_2Slave
        self.step = 0

    def next(self):
        self.step = (self.step +1) % len(self.fsm)
        print "-----> %s -----> state chang to %d\n" % (self.fsm, self.step)

    def stat(self):
        return self.fsm[self.step]

