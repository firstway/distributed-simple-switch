


'''
seq | type | name | key:value, key:value, ....
'''

PEER_MSG_CALL_IN = 90

PEER_MSG_INIT_Q = 100 # send this msg if the controller is the Master role
PEER_MSG_INIT_Reject = 101 #
PEER_MSG_INIT_Agree = 102 #

PEER_MSG_ROLE_CHANGE = 110
PEER_MSG_EXCEPT_FLOW_DEL = 111


PEER_MSG_ASK_CHANGE_TO_MASTER = 200

'Error or Recovery msg'
PEER_MSG_RESET = 501

class PeerMsg(object):
    def __init__(self, name, _type, info):
        self.name = name 
        self.msg_type = _type
        self.info = info

    def __str__(self):
        return encode_msg(self)

def encode_msg(msg_obj):
    return '%d|%d|%s|%s\n' % (1, msg_obj.msg_type, msg_obj.name, ','.join(['%s:%s' %(k,v) for k,v in msg_obj.info.items() ]))

def decode_msg(sline):
    ps = sline.split('|')
    seq = int(ps[0])
    _type = int(ps[1])
    name = ps[2]
    info = {}
    for kv in ps[3].split(","):
        kv = kv.strip()
        if kv:
            k,v = kv.split(":")
            info[k] = v
    return PeerMsg(name, _type, info)

if __name__ == '__main__':
    msg = PeerMsg('controller', PEER_MSG_INIT_Q, {})
    print "[%s]\n" % encode_msg(msg)

