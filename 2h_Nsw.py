from mininet.topo import Topo

N = 20 

class MyTopo( Topo ):
    "Simple topology example."

    def __init__( self ):
        "Create custom topo."

        # Initialize topology
        Topo.__init__( self )

        n = N
        # Add hosts and switches
        leftHost = self.addHost( 'h1' )
        rightHost = self.addHost( 'h2' )
        sw_list = []
        for i in xrange(n):
            _id = i+3
            sw_list.append( self.addSwitch( 's'+ str(_id)) )

        # Add links
        self.addLink( leftHost, sw_list[0] )
        self.addLink( sw_list[-1], rightHost )
        for i in xrange(n-1):
            self.addLink( sw_list[i] , sw_list[i+1])


topos = { 'mytopo': ( lambda: MyTopo() ) }

