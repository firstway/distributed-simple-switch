


import contextlib
import logging
import random
import eventlet
from oslo.config import cfg

from ryu.lib.hub import StreamServer
from ryu.lib import hub
from ryu.controller.controller import CONF 
from ryu.base import app_manager

import peer_event

LOG = logging.getLogger('ryu.controller.ControllerPeers')


class ControllerPeers(object):
    def __init__(self):
        super(ControllerPeers, self).__init__()

    # entry point
    def __call__(self):
        #LOG.debug('call')
        self.server_loop()

    def server_loop(self):
    	server = StreamServer(("0.0.0.0",6737), controller_peers_connection_factory)
        #LOG.debug('loop')
        server.serve_forever()

class ControllerPeerClient(object):
    def __init__(self, peer_ip, peer_port):
        super(ControllerPeerClient, self).__init__()
        self.peer_ip = peer_ip
        self.peer_port = peer_port

    # entry point
    def __call__(self):
        #LOG.debug('call')
        self.connect_server()

    def connect_server(self):
        controller_peers_connection_factory(eventlet.connect((self.peer_ip, self.peer_port)), self.peer_ip)


class ThePeer(object):
    def __init__(self, socket, address):
        super(ThePeer, self).__init__()

        self.socket = socket
        self.address = address
        self.is_active = True
        self.appman = app_manager.AppManager.get_instance()

        # The limit is arbitrary. We need to limit queue size to
        # prevent it from eating memory up
        self.send_q = hub.Queue(16)

    def close(self):
        LOG.debug("ThePeer will close") 

    def __str__(self):
        return str(self.socket)

    def serve(self):
        send_thr = hub.spawn(self._send_loop)
        call_in_thr = hub.spawn(self._broadcast)
        try:
            self._recv_loop()
        finally:
            hub.kill(send_thr)
            hub.kill(call_in_thr)
            hub.joinall([send_thr, call_in_thr])

    def _broadcast(self):
        while True:
            eventlet.sleep(random.randint(3,6))
            main_ctr = self.appman.applications['DistributedSimpleSwitch']
            main_ctr.broadcast(self)
        
    def _send_loop(self):
        try:
            while self.is_active:
                buf = self.send_q.get()
                self.socket.sendall(buf)
        finally:
            q = self.send_q
            # first, clear self.send_q to prevent new references.
            self.send_q = None
            # there might be threads currently blocking in send_q.put().
            # unblock them by draining the queue.
            try:
                while q.get(block=False):
                    pass
            except hub.QueueEmpty:
                pass

    def _recv_loop(self):
        ret = ''
        while self.is_active:
            ret += self.socket.recv(512)
            if not ret:
                LOG.debug('connection is going to shutdown')
                break
            #print "peer recv:[%s]\n" % ret
            ind = ret.find('\n')
            if ind >= 0:
                bloc = ret[:ind] 
                ret = ret[ind+1:]
                self.appman.applications['DistributedSimpleSwitch'].react_to_peer(self, peer_event.decode_msg(bloc))
            #print ">>>",self.appman.applications.keys()

    def send_msg(self, msg_obj):
        if self.send_q:
            self.send_q.put(peer_event.encode_msg(msg_obj))

    def __str__(self):
        return  str(self.socket) + str(self.address)


def controller_peers_connection_factory(socket, address):
    LOG.debug('connected socket:%s address:%s', socket, address)
    with contextlib.closing(ThePeer(socket, address)) as peer:
        try:
            peer.serve()
        except:
            # Something went wrong.
            # Especially malicious switch can send malformed packet,
            # the parser raise exception.
            # Can we do anything more graceful?
            LOG.error("Error in the peer ")
            raise


